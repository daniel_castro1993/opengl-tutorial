#include "OpenGL_incs.hpp"

#include "GLRenderer.hpp"
#include "GLLogger.hpp"
#include "shaders/shaderlib.h"
#include "mesh/GLMesh.hpp"

#include "glm/glm.hpp"

#include <stdio.h>
#include <stdlib.h>

#define BUFFER_OFFSET(i) ((char*)NULL + (i))

using namespace tuto1;

GLchar* readFile(const char* fileName) {
  FILE* fp = fopen(fileName, "r");
  long size;
  GLchar* res;

  fseek(fp, 0, SEEK_END);
  size = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  res = (GLchar*) malloc(sizeof (GLchar) * size);

  fread(res, sizeof (GLchar), size, fp);

  return res;
}

void glfw_error_callback(int error, const char* description) {
  GLLogger* logger = GLLogger::instance();
  logger->SetDefaultLevel(GLLoggerLevel::ERROR);

  logger->Log("GLFW ERROR: code %i msg: %s\n", error, description);
}

void _update_fps_counter(GLFWwindow* window) {
  static double previous_seconds = glfwGetTime();
  static int frame_count;
  double current_seconds = glfwGetTime();
  double elapsed_seconds = current_seconds - previous_seconds;
  if (elapsed_seconds > 0.25) {
    previous_seconds = current_seconds;
    double fps = (double) frame_count / elapsed_seconds;
    char tmp[128];
    sprintf(tmp, "opengl @ fps: %.2f", fps);
    glfwSetWindowTitle(window, tmp);
    frame_count = 0;
  }
  frame_count++;
}

int main() {
  GLLogger* logger = GLLogger::instance();
  GLRenderer* loop = GLRenderer::instance();

  loop->Prepare();
  loop->SetWindowTitle("Hello world!");

  GLMesh mesh;

  mesh.SetUseIndices(false);
  
  mesh.AddVertex(vec3(0.0, 0.85, 0.0));
  mesh.AddVertex(vec3(-0.75, -0.75, 0.0));
  mesh.AddVertex(vec3(0.5, -0.5, 0.0));

  mesh.AddIndex(0);
  mesh.AddIndex(1);
  mesh.AddIndex(2);

  mesh.GenerateVBO();
  mesh.GenerateVAO();

  list<string> vert_shaders;
  //  vert_shaders.push_back("./shaders/broken_shader.glsl");
  //  vert_shaders.push_back("./shaders/vs1_v400.glsl");
  vert_shaders.push_back("./shaders/vs_v330.glsl");

  list<string> frag_shaders;
  //  frag_shaders.push_back("./shaders/broken_shader.glsl");
  //  frag_shaders.push_back("./shaders/fs1_v400.glsl");
  //  frag_shaders.push_back("./shaders/fs2_v400.glsl");
  frag_shaders.push_back("./shaders/fs_v330.glsl");

  GLShaderManager* manager = GLShaderManager::instance();
  GLShaderProgram* program1 = manager->NewProgram("s1");

  program1->SetVertexShader(vert_shaders);
  program1->SetFragmentShader(frag_shaders);
  program1->Compile();

  vec4 color1(1.0, 0.2, 0.8, 0.3);

  program1->Use();
  program1->SetUniform("u_color", color1);
  program1->SetUniform("u_time", 1221.123f);

  //  glfwSwapInterval(1);

  loop->AddCallback("UpdateFrames", []
          (GLRenderer& rend, double elapsed) -> void {
            _update_fps_counter(rend.GetWindow());
          });

  loop->AddCallback("PrintTriangle", [&mesh]
          (GLRenderer& rend, double elapsed) -> void {
            static double counter = 0;
            GLShaderManager* manager = GLShaderManager::instance();
            GLShaderProgram* program = manager->GetProgram("s1");
            GLFWwindow* window = rend.GetWindow();
            int width, height;

            counter += elapsed;

            glfwGetFramebufferSize(window, &width, &height);

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glViewport(0, 0, width, height);
            program->Use();
            program->SetUniform("u_time", (GLfloat) counter);
            //        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vao);
            //        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_SHORT, BUFFER_OFFSET(0));
            //        glBindVertexArray(vao);
            //        glDrawArrays(GL_TRIANGLES, 0, 3);
            mesh.Draw();
            glfwPollEvents();
            glfwSwapBuffers(window);
          });

  loop->AddCallback("CheckExit", []
          (GLRenderer& rend, double elapsed) -> void {
            if (GLFW_PRESS == glfwGetKey(rend.GetWindow(), GLFW_KEY_ESCAPE)) {
              glfwSetWindowShouldClose(rend.GetWindow(), 1);
            }
            if (glfwWindowShouldClose(rend.GetWindow())) {
              rend.Exit();
            }
          });

  loop->AddCallback("ToggleFullscreen", []
          (GLRenderer& rend, double elapsed) -> void {
            if (GLFW_PRESS == glfwGetKey(rend.GetWindow(), GLFW_KEY_F)) {
              rend.SetFullscreen();
            }
            if (GLFW_PRESS == glfwGetKey(rend.GetWindow(), GLFW_KEY_W)) {
              rend.SetWindowed();
            }
          });

  // calls the previous callbacks over and over  
  loop->MainLoop();

  manager->DelProgram("s1");

  // close GL context and any other GLFW resources
  glfwTerminate();

  logger->SetDefaultLevel(GLLoggerLevel::INFO);
  logger->Log("Exit success!");

  return 0;
}
