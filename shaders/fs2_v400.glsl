#version 400

out vec4 frag_colour;

uniform vec4 color;

void main () {
  frag_colour = 2 * color;
}
