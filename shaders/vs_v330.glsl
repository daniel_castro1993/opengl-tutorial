#version 330

out vec4 o_position;
// out vec3 o_normal;
// out vec2 o_texcoord;

in vec3 i_position;
// in vec3 i_normal;
// in vec2 i_texcoord;

void main () {
  // o_position = vec4(i_position, 1.0);
  // o_normal = i_normal;
  // o_texcoord = i_texcoord;
  gl_Position = vec4(i_position, 1.0);
}

