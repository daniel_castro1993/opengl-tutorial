#version 400

out vec4 o_frag_colour;

in vec3 o_position;
in vec3 o_normal;
in vec2 o_texcoord;

uniform vec4 u_color;
uniform float u_time;

@include "./shaders/frag_inc.glsl"

void main () {
  vec4 n_pos = vec4(normalize(o_position), 1.0);
  vec4 n_nor = vec4(o_normal, 1.0);
  vec4 v_col = mix(u_color, n_nor, n_pos);
  o_frag_colour = mix(v_col, _frag_lib_varColor(u_time), n_pos);
}
