#version 400

out vec3 o_position;
out vec3 o_normal;
out vec2 o_texcoord;

in vec3 i_position;
in vec3 i_normal;
in vec2 i_texcoord;

void main () {
  // float n_len = length(i_normal);
  o_position = i_position;
  // o_normal = !(n_len > 0.0) ? vec3(1.0, 0.0, 0.0) : i_normal / len;
  o_normal = i_position;
  o_texcoord = i_texcoord;
  gl_Position = vec4(i_position, 1.0);
}
