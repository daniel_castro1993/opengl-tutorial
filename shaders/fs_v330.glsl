#version 330

// out vec4 frag_color;

// in vec4 o_position;
// in vec3 o_normal;
// in vec2 o_texcoord;

uniform vec4 u_color;
uniform float u_time;

@include "./shaders/frag_inc.glsl"

void main () {
  // vec4 v_color = _frag_lib_varColor(u_time);
  // vec4 n_color = mix(u_color, v_color, o_position);
  // gl_FragColor = n_color;
  gl_FragColor = u_color;
}
