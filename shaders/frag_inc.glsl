
vec4 _frag_lib_varColor(float _time) {
  float _t = 2 + _time * 1000;
  return vec4 (cos(_t), sin(_t), 1 - cos(_t), 1.0);
}

vec4 _frag_lib_varColorV2(float _time) {
  int _arg = int(_time * 10000.0f);
  float _num = float(_arg % 1000);
  float _t = _num / 1000.0f;
  return vec4 (_t, _t, 1 - _t, 1.0);
}

