#ifndef GLMESH_HPP
#define GLMESH_HPP

#include "OpenGL_incs.hpp"
#include "glm/glm.hpp"

#include <vector>

using namespace std;
using namespace glm;

namespace tuto1 {

  typedef struct _vertex_t {
    vec3 position;
    vec3 normal;
    vec2 texcoord;
  } vertex_t;

  class GLMesh {
  public:
    GLMesh();
    GLMesh(const GLMesh& orig);
    virtual ~GLMesh();

    void SetUseIndices(bool useIndices);

    void AddVertex(vec3 pos, vec3 nor = vec3(0.0, 0.0, 0.0),
            vec2 tex = vec2(0.0, 0.0));
    void AddVertex(vertex_t vertex);

    void AddIndex(GLushort);

    void GenerateVBO();
    void GenerateVAO();

    void Draw();

  private:

    GLuint _vertVBO, _indxVBO, _VAO;
    vector<vertex_t> _vertices;
    vector<GLushort> _indices;

    bool _useIndices;
  };
}

#endif /* GLMESH_HPP */

