#ifndef GLLOGGER_HPP
#define GLLOGGER_HPP

#include <string>

#define GLLOGGER_OUTPUT_FILE "gl.log"

using namespace std;

namespace tuto1 {
  
  enum class GLLoggerLevel { INFO = 0, WARNING = 20, ERROR = 100};
  
  typedef enum _GL_LOGGER_ENUM {
    
  } GL_LOGGER_ENUM;
  
  class GLLogger {
  public:
    
    static GLLogger* instance();
    
    
    void SetOutputFile(string fileName);
    void SetDefaultLevel(GLLoggerLevel level);
    GLLoggerLevel GetDefaultLevel();
    void SetPrintToConsole(bool printStdOut);
    bool GetPrintToConsole();
    bool Log(const char* message, ...);
    
  protected:
    GLLogger(string fileName);
    ~GLLogger();
    
  private:
    
    FILE* _fp;
    string _fileName;
    bool _printStdOut;
    GLLoggerLevel _defaultLevel;
    

  };
}

#endif /* GLLOGGER_HPP */

