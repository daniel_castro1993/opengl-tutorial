#ifndef GLRENDERER_HPP
#define GLRENDERER_HPP

#include "OpenGL_incs.hpp"
#include "GLLogger.hpp"

#include <functional>
#include <list>
#include <map>

#define __WINDOWED_DEFAULT_WIDTH__  640
#define __WINDOWED_DEFAULT_HEIGHT__ 480

using namespace std;

namespace tuto1 {

  class GLRenderer {
  public:

    static GLRenderer* instance();


    bool Prepare();

    void AddLoadingCallback(string name, function<void(GLRenderer&) > callback);

    void AddCallback(string name, function<void(GLRenderer&,
            double) > callback);

    list<string>& GetVideoModes();

    bool SetWindowed(int width = __WINDOWED_DEFAULT_WIDTH__,
            int height = __WINDOWED_DEFAULT_HEIGHT__);
    bool SetFullscreen();
    void SetWindowTitle(string title);

    GLFWwindow* GetWindow();

    void MainLoop();
    void Exit();

  protected:
    GLRenderer();
    ~GLRenderer();
    
  private:

    map<string, function<void(GLRenderer&, double) > > _callbacks;
    map<string, function<void(GLRenderer&) > > _loads;
    double _lastTS;

    bool _isExit;

    GLFWmonitor* _monitor;
    GLFWwindow* _window;
    const GLFWvidmode* _vmode;
    list<string> _videoModes;

    string _title;

    GLLogger* _logger;
  };
}

#endif /* GLRENDERER_HPP */
