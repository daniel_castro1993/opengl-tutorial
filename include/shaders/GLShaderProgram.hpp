#ifndef GLSHADERPROGRAM_HPP
#define GLSHADERPROGRAM_HPP

#include "OpenGL_incs.hpp"
#include "shaders/GLShaders.hpp"
#include "GLLogger.hpp"

#include "glm/glm.hpp"

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>

using namespace std;
using namespace glm;

namespace tuto1 {

  class GLShaderProgram {
    friend class GLShaderManager;
  public:

    GLShaderProgram(string name);
    GLShaderProgram(const GLShaderProgram& orig);
    virtual ~GLShaderProgram();

    /**
     * Also compiles the shaders.
     */
    bool SetVertexShader(list<string>& vertexShaderFileName);
    bool SetFragmentShader(list<string>& fragmentShaderFileName);
    void Compile();
    void Delete();
    void Use();
    bool IsValid();
    void PrintStatus();
    string GetName();


    void SetUniform(string name, GLint int1);
    void SetUniform(string name, ivec2 int2);
    void SetUniform(string name, ivec3 int3);
    void SetUniform(string name, ivec4 int4);

    void SetUniform(string name, vector<GLint> int1v);
    void SetUniform(string name, vector<ivec2> int2v);
    void SetUniform(string name, vector<ivec3> int3v);
    void SetUniform(string name, vector<ivec4> int4v);

    void SetUniform(string name, GLuint uint1);
    void SetUniform(string name, uvec2 uint2);
    void SetUniform(string name, uvec3 uint3);
    void SetUniform(string name, uvec4 uint4);

    void SetUniform(string name, vector<GLuint> uint1v);
    void SetUniform(string name, vector<uvec2> uint2v);
    void SetUniform(string name, vector<uvec3> uint3v);
    void SetUniform(string name, vector<uvec4> uint4v);

    void SetUniform(string name, GLfloat float1);
    void SetUniform(string name, vec2 float2);
    void SetUniform(string name, vec3 float3);
    void SetUniform(string name, vec4 float4);

    void SetUniform(string name, vector<GLfloat> float1v);
    void SetUniform(string name, vector<vec2> float2v);
    void SetUniform(string name, vector<vec3> float3v);
    void SetUniform(string name, vector<vec4> float4v);

    void SetUniform(string name, GLdouble double1);
    void SetUniform(string name, dvec2 double2);
    void SetUniform(string name, dvec3 double3);
    void SetUniform(string name, dvec4 double4);

    void SetUniform(string name, vector<GLdouble> double1v);
    void SetUniform(string name, vector<dvec2> double2v);
    void SetUniform(string name, vector<dvec3> double3v);
    void SetUniform(string name, vector<dvec4> double4v);


    void SetUniform(string name, vector<mat2x2> matrix, bool makeTranspose);
    void SetUniform(string name, vector<mat2x3> matrix, bool makeTranspose);
    void SetUniform(string name, vector<mat2x4> matrix, bool makeTranspose);
    void SetUniform(string name, vector<mat3x2> matrix, bool makeTranspose);
    void SetUniform(string name, vector<mat3x3> matrix, bool makeTranspose);
    void SetUniform(string name, vector<mat3x4> matrix, bool makeTranspose);
    void SetUniform(string name, vector<mat4x2> matrix, bool makeTranspose);
    void SetUniform(string name, vector<mat4x3> matrix, bool makeTranspose);
    void SetUniform(string name, vector<mat4x4> matrix, bool makeTranspose);

    void SetUniform(string name, vector<dmat2x2> matrix, bool makeTranspose);
    void SetUniform(string name, vector<dmat2x3> matrix, bool makeTranspose);
    void SetUniform(string name, vector<dmat2x4> matrix, bool makeTranspose);
    void SetUniform(string name, vector<dmat3x2> matrix, bool makeTranspose);
    void SetUniform(string name, vector<dmat3x3> matrix, bool makeTranspose);
    void SetUniform(string name, vector<dmat3x4> matrix, bool makeTranspose);
    void SetUniform(string name, vector<dmat4x2> matrix, bool makeTranspose);
    void SetUniform(string name, vector<dmat4x3> matrix, bool makeTranspose);
    void SetUniform(string name, vector<dmat4x4> matrix, bool makeTranspose);

  private:

    GLLogger* _logger;

    GLuint _vert, _frag, _program;
    bool _isVertSet, _isFragSet;

    string _name;

    string _vertexShaderFileName, _fragmentShaderFileName;
    string _vertexShader, _fragmentShader;

    map<string, GLint> _uniforms;

    void SetShader(list<string>& paths, string& shaderName,
            string& shaderSource, GLuint& id, bool& isSet,
            GLenum shaderType);

    GLint GetUniformLocation(string name);
  };

}

#endif /* GLSHADERPROGRAM_HPP */

