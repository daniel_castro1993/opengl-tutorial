#ifndef GLSHADERMANAGER_HPP
#define GLSHADERMANAGER_HPP

#include "OpenGL_incs.hpp"
#include "shaders/GLShaders.hpp"
#include "GLShaderProgram.hpp"

#include <map>
#include <utility>

using namespace std;

namespace tuto1 {

  class GLShaderManager {
    friend class GLShaderProgram;
  public:

    static GLShaderManager* instance();

    GLShaderProgram* NewProgram(string name);
    GLShaderProgram* GetProgram(string name);
    void DelProgram(string name);
    
  protected:
    GLShaderManager();
    ~GLShaderManager();
    
  private:

    map<string, pair<GLuint, int> > _shaders;
    map<string, GLShaderProgram*> _programs;
    
    void AddShader(string path, GLuint vert);
    bool GetShader(string path, GLuint& vert);
    void DelShader(string path);

  };
}

#endif /* GLSHADERMANAGER_HPP */

