#include "shaders/shaderlib.h"
#include "GLLogger.hpp"
#include "OpenGL_incs.hpp"

#include "glm/gtc/type_ptr.hpp"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <regex>
#include <string.h>

using namespace tuto1;
using namespace std;
using namespace glm;

static string readShader(string path);
static string readFile(string path);
static void printShaderInfoLog(GLuint shader_index, GLLogger* logger);
static void printProgramInfoLog(GLuint program, GLLogger* logger);
static void printAll(GLuint program, GLLogger* logger);
static const char* GLTypeToString(GLenum type);
static size_t bufferline(char* lineptr, size_t n, FILE* stream);

static GLShaderManager* _shaderManager = GLShaderManager::instance();

GLShaderProgram::GLShaderProgram(std::string name)
: _isVertSet(false), _isFragSet(false),
_logger(GLLogger::instance()) {
  _name = name;
}

GLShaderProgram::GLShaderProgram(const GLShaderProgram& orig)
: _logger(GLLogger::instance()) {

  list<string> vsFileName;
  list<string> fsFileName;

  _name = orig._name;
  vsFileName.push_back(orig._vertexShaderFileName);
  fsFileName.push_back(orig._fragmentShaderFileName);

  SetVertexShader(vsFileName);
  SetFragmentShader(fsFileName);
}

GLShaderProgram::~GLShaderProgram() {
  Delete();
}

bool GLShaderProgram::SetVertexShader(list<string>& vertexShaderFileName) {

  SetShader(vertexShaderFileName, _vertexShaderFileName,
      _vertexShader, _vert, _isVertSet, GL_VERTEX_SHADER);
}

bool GLShaderProgram::SetFragmentShader(list<string>& fragmentShaderFileName) {

  SetShader(fragmentShaderFileName, _fragmentShaderFileName,
      _fragmentShader, _frag, _isFragSet, GL_FRAGMENT_SHADER);
}

void GLShaderProgram::Compile() {
  int params = -1;

  _logger->SetDefaultLevel(GLLoggerLevel::ERROR);

  _program = glCreateProgram();

  if (_isVertSet) {
    glAttachShader(_program, _vert);
  }

  if (_isFragSet) {
    glAttachShader(_program, _frag);
  }

  glLinkProgram(_program);
  glGetProgramiv(_program, GL_LINK_STATUS, &params);
  if (GL_TRUE != params) {
    _logger->Log("Could not link shader program GL index %u", _program);
    printProgramInfoLog(_program, _logger);
  }

  if (_isVertSet) {
    glDetachShader(_program, _vert);
  }

  if (_isFragSet) {
    glDetachShader(_program, _frag);
  }
}

void GLShaderProgram::Delete() {

  if (_isVertSet) {
    _shaderManager->DelShader(_vertexShaderFileName);
    _isVertSet = false;
  }

  if (_isFragSet) {
    _shaderManager->DelShader(_fragmentShaderFileName);
    _isFragSet = false;
  }
  glDeleteProgram(_program);
}

void GLShaderProgram::Use() {
  glUseProgram(_program);
}

bool GLShaderProgram::IsValid() {
  int params = -1;
  glValidateProgram(_program);
  glGetProgramiv(_program, GL_VALIDATE_STATUS, &params);
  _logger->SetDefaultLevel(GLLoggerLevel::INFO);
  _logger->Log("program %i GL_VALIDATE_STATUS = %i", _program, params);
  if (GL_TRUE != params) {
    printProgramInfoLog(_program, _logger);
    return false;
  }
  return true;
}

void GLShaderProgram::PrintStatus() {
  bool printToConsole = _logger->GetPrintToConsole();
  _logger->SetPrintToConsole(true);
  _logger->SetDefaultLevel(GLLoggerLevel::INFO);
  printAll(_program, _logger);
  _logger->SetPrintToConsole(printToConsole);
}

string GLShaderProgram::GetName() {
  return _name;
}

// INTs

void GLShaderProgram::SetUniform(string name, GLint i1) {
  glUniform1i(GetUniformLocation(name), i1);
}

void GLShaderProgram::SetUniform(string name, ivec2 i2) {
  glUniform2i(GetUniformLocation(name), i2.x, i2.y);
}

void GLShaderProgram::SetUniform(string name, ivec3 i3) {
  glUniform3i(GetUniformLocation(name), i3.x, i3.y, i3.z);
}

void GLShaderProgram::SetUniform(string name, ivec4 i4) {
  glUniform4i(GetUniformLocation(name), i4.x, i4.y, i4.z, i4.w);
}

// INTs vector version

void GLShaderProgram::SetUniform(string name, vector<GLint> vec) {
  glUniform1iv(GetUniformLocation(name), vec.size(), &vec[0]);
}

void GLShaderProgram::SetUniform(string name, vector<ivec2> vec) {
  glUniform2iv(GetUniformLocation(name), vec.size(),
      static_cast<GLint*> (value_ptr(vec.front())));
}

void GLShaderProgram::SetUniform(string name, vector<ivec3> vec) {
  glUniform3iv(GetUniformLocation(name), vec.size(),
      static_cast<GLint*> (value_ptr(vec.front())));
}

void GLShaderProgram::SetUniform(string name, vector<ivec4> vec) {
  glUniform4iv(GetUniformLocation(name), vec.size(),
      static_cast<GLint*> (value_ptr(vec.front())));
}

// UINTs

void GLShaderProgram::SetUniform(string name, GLuint i1) {
  glUniform1ui(GetUniformLocation(name), i1);
}

void GLShaderProgram::SetUniform(string name, uvec2 i2) {
  glUniform2ui(GetUniformLocation(name), i2.x, i2.y);
}

void GLShaderProgram::SetUniform(string name, uvec3 i3) {
  glUniform3ui(GetUniformLocation(name), i3.x, i3.y, i3.z);
}

void GLShaderProgram::SetUniform(string name, uvec4 i4) {
  glUniform4ui(GetUniformLocation(name), i4.x, i4.y, i4.z, i4.w);
}

// UINTs vector version

void GLShaderProgram::SetUniform(string name, vector<GLuint> vec) {
  glUniform1uiv(GetUniformLocation(name), vec.size(), &vec[0]);
}

void GLShaderProgram::SetUniform(string name, vector<uvec2> vec) {
  glUniform2uiv(GetUniformLocation(name), vec.size(),
      static_cast<GLuint*> (value_ptr(vec.front())));
}

void GLShaderProgram::SetUniform(string name, vector<uvec3> vec) {
  glUniform3uiv(GetUniformLocation(name), vec.size(),
      static_cast<GLuint*> (value_ptr(vec.front())));
}

void GLShaderProgram::SetUniform(string name, vector<uvec4> vec) {
  glUniform4uiv(GetUniformLocation(name), vec.size(),
      static_cast<GLuint*> (value_ptr(vec.front())));
}

// FLOATS

void GLShaderProgram::SetUniform(string name, GLfloat i1) {
  glUniform1f(GetUniformLocation(name), i1);
}

void GLShaderProgram::SetUniform(string name, vec2 i2) {
  glUniform2f(GetUniformLocation(name), i2.x, i2.y);
}

void GLShaderProgram::SetUniform(string name, vec3 i3) {
  glUniform3f(GetUniformLocation(name), i3.x, i3.y, i3.z);
}

void GLShaderProgram::SetUniform(string name, vec4 i4) {
  glUniform4f(GetUniformLocation(name), i4.x, i4.y, i4.z, i4.w);
}

// FLOATS vector version

void GLShaderProgram::SetUniform(string name, vector<GLfloat> vec) {
  glUniform1fv(GetUniformLocation(name), vec.size(), &vec[0]);
}

void GLShaderProgram::SetUniform(string name, vector<vec2> vec) {
  glUniform2fv(GetUniformLocation(name), vec.size(),
      static_cast<GLfloat*> (value_ptr(vec.front())));
}

void GLShaderProgram::SetUniform(string name, vector<vec3> vec) {
  glUniform3fv(GetUniformLocation(name), vec.size(),
      static_cast<GLfloat*> (value_ptr(vec.front())));
}

void GLShaderProgram::SetUniform(string name, vector<vec4> vec) {
  glUniform4fv(GetUniformLocation(name), vec.size(),
      static_cast<GLfloat*> (value_ptr(vec.front())));
}

// DOUBLES

void GLShaderProgram::SetUniform(string name, GLdouble i1) {
  glUniform1d(GetUniformLocation(name), i1);
}

void GLShaderProgram::SetUniform(string name, dvec2 i2) {
  glUniform2d(GetUniformLocation(name), i2.x, i2.y);
}

void GLShaderProgram::SetUniform(string name, dvec3 i3) {
  glUniform3d(GetUniformLocation(name), i3.x, i3.y, i3.z);
}

void GLShaderProgram::SetUniform(string name, dvec4 i4) {
  glUniform4d(GetUniformLocation(name), i4.x, i4.y, i4.z, i4.w);
}

// DOUBLES vector version

void GLShaderProgram::SetUniform(string name, vector<GLdouble> vec) {
  glUniform1dv(GetUniformLocation(name), vec.size(), &vec[0]);
}

void GLShaderProgram::SetUniform(string name, vector<dvec2> vec) {
  glUniform2dv(GetUniformLocation(name), vec.size(),
      static_cast<GLdouble*> (value_ptr(vec.front())));
}

void GLShaderProgram::SetUniform(string name, vector<dvec3> vec) {
  glUniform3dv(GetUniformLocation(name), vec.size(),
      static_cast<GLdouble*> (value_ptr(vec.front())));
}

void GLShaderProgram::SetUniform(string name, vector<dvec4> vec) {
  glUniform4dv(GetUniformLocation(name), vec.size(),
      static_cast<GLdouble*> (value_ptr(vec.front())));
}

// FLOAT MATRICES

void GLShaderProgram::SetUniform(string name, vector<mat2x2> matrix, bool t) {
  glUniformMatrix2fv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLfloat*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<mat2x3> matrix, bool t) {
  glUniformMatrix2x3fv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLfloat*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<mat2x4> matrix, bool t) {
  glUniformMatrix2x4fv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLfloat*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<mat3x2> matrix, bool t) {
  glUniformMatrix3x2fv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLfloat*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<mat3x3> matrix, bool t) {
  glUniformMatrix3fv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLfloat*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<mat3x4> matrix, bool t) {
  glUniformMatrix3x4fv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLfloat*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<mat4x2> matrix, bool t) {
  glUniformMatrix4x2fv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLfloat*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<mat4x3> matrix, bool t) {
  glUniformMatrix4x3fv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLfloat*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<mat4x4> matrix, bool t) {
  glUniformMatrix4fv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLfloat*> (value_ptr(matrix.front())));
}

// DOUBLE MATRICES

void GLShaderProgram::SetUniform(string name, vector<dmat2x2> matrix, bool t) {
  glUniformMatrix2dv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLdouble*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<dmat2x3> matrix, bool t) {
  glUniformMatrix2x3dv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLdouble*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<dmat2x4> matrix, bool t) {
  glUniformMatrix2x4dv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLdouble*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<dmat3x2> matrix, bool t) {
  glUniformMatrix3x2dv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLdouble*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<dmat3x3> matrix, bool t) {
  glUniformMatrix3dv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLdouble*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<dmat3x4> matrix, bool t) {
  glUniformMatrix3x4dv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLdouble*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<dmat4x2> matrix, bool t) {
  glUniformMatrix4x2dv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLdouble*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<dmat4x3> matrix, bool t) {
  glUniformMatrix4x3dv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLdouble*> (value_ptr(matrix.front())));
}

void GLShaderProgram::SetUniform(string name, vector<dmat4x4> matrix, bool t) {
  glUniformMatrix4dv(GetUniformLocation(name), matrix.size(), t,
      static_cast<GLdouble*> (value_ptr(matrix.front())));
}

GLint GLShaderProgram::GetUniformLocation(string name) {
  map<string, GLint>::iterator it;

  it = _uniforms.find(name);

  if (it != _uniforms.end()) {
    return it->second;
  } else {
    glUseProgram(_program);
    GLint loc = glGetUniformLocation(_program, name.c_str());
    _uniforms[name] = loc;
    assert(loc != -1);
    return loc;
  }
}

static string readShader(string path) {
  string res;
  FILE* fp = fopen(path.c_str(), "r");
  char readline[2048];
  size_t sizeReadline = 2048;
  stringstream consShader;

  if (fp == NULL) {
    return "";
  }

  while (bufferline(readline, sizeReadline, fp) != -1) {
    string line = readline;
    size_t pos = line.find("@include");

    if (pos != string::npos) {
      // this line is a include directive
      size_t posFirstQuote = line.find("\"", pos);

      if (posFirstQuote != string::npos) {
        size_t posSecondQuote = line.find("\"", posFirstQuote + 1);

        if (posSecondQuote != string::npos) {
          // now we get the file name
          string includeFile = line.substr(posFirstQuote + 1,
              posSecondQuote - posFirstQuote - 1);
          string incSource = readFile(includeFile);
          size_t lastNewline = incSource.find_last_of("\n");
          incSource = incSource.substr(0, lastNewline);

          line = incSource.append("\n");
        }
      }
    }

    consShader << line;
  }

  res = consShader.str();
  fclose(fp);

  return res;
}

void GLShaderProgram::SetShader(list<string>& paths, string& shaderName,
    string& shaderSource, GLuint& id, bool& isSet, GLenum shaderType) {

  int params = -1;
  list<string>::iterator it;

  for (it = paths.begin(); it != paths.end(); ++it) {
    string path = *it;

    _logger->SetDefaultLevel(GLLoggerLevel::INFO);
    _logger->Log(" --- Trying shader \"%s\"", path.c_str());

    shaderName = path;
    shaderSource = readShader(path);

    if (!_shaderManager->GetShader(path, id)) {
      GLchar shader[shaderSource.size()];
      const GLchar* s = (const GLchar*) shader;

      // not found
      strncpy(shader, shaderSource.c_str(), shaderSource.size());
      id = glCreateShader(shaderType);
      glShaderSource(id, 1, &s, NULL);
      glCompileShader(id);
      glGetShaderiv(id, GL_COMPILE_STATUS, &params);

      if (params == GL_TRUE) {
        _logger->Log(" SUCCESS!");
        _logger->SetDefaultLevel(GLLoggerLevel::INFO);
        _logger->Log("GL shader \"%s\" id: %i", path.c_str(), id);
        _shaderManager->AddShader(path, id);
        break;
      } else {
        _logger->Log(" FAILED!");
      }
    }
  }

  if (params != GL_TRUE) {
    // error none of the provided shaders actually worked
    _logger->SetDefaultLevel(GLLoggerLevel::ERROR);
    _logger->Log("The provided shaders did not compile. Last shader info:");
    printShaderInfoLog(id, _logger);
    _logger->Log(" --- Last shader source:\n%s", shaderSource.c_str());
    isSet = false;
  } else {
    isSet = true;
  }
}

static string readFile(string path) {
  string res;
  FILE* fp = fopen(path.c_str(), "r");
  int size;

  fseek(fp, 0, SEEK_END);
  size = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  char fragDump[size];

  fread(fragDump, sizeof (GLchar), size, fp);

  res = fragDump;

  fclose(fp);

  return res;
}

static void printShaderInfoLog(GLuint shader_index, GLLogger* logger) {
  int max_length = 2048;
  int actual_length = 0;
  char log[2048];
  glGetShaderInfoLog(shader_index, max_length, &actual_length, log);
  logger->Log("Shader info log for GL index %u:\n%s", shader_index, log);
}

static void printProgramInfoLog(GLuint program, GLLogger* logger) {
  int max_length = 2048;
  int actual_length = 0;
  char log[2048];
  glGetProgramInfoLog(program, max_length, &actual_length, log);
  logger->Log("Program info log for GL index %u:\n%s", program, log);
}

static void printAll(GLuint program, GLLogger* logger) {
  logger->Log("--------------------\nshader program %i info:", program);
  int params = -1;
  glGetProgramiv(program, GL_LINK_STATUS, &params);
  logger->Log("GL_LINK_STATUS = %i", params);

  glGetProgramiv(program, GL_ATTACHED_SHADERS, &params);
  logger->Log("GL_ATTACHED_SHADERS = %i", params);

  glGetProgramiv(program, GL_ACTIVE_ATTRIBUTES, &params);
  logger->Log("GL_ACTIVE_ATTRIBUTES = %i", params);
  for (int i = 0; i < params; i++) {
    char name[64];
    int max_length = 64;
    int actual_length = 0;
    int size = 0;
    GLenum type;
    glGetActiveAttrib(
        program,
        i,
        max_length,
        &actual_length,
        &size,
        &type,
        name
        );
    if (size > 1) {
      for (int j = 0; j < size; j++) {
        char long_name[64];
        sprintf(long_name, "%s[%i]", name, j);
        int location = glGetAttribLocation(program, long_name);
        logger->Log("  %i) type:%s name:%s location:%i",
            i, GLTypeToString(type), long_name, location);
      }
    } else {
      int location = glGetAttribLocation(program, name);
      logger->Log("  %i) type:%s name:%s location:%i",
          i, GLTypeToString(type), name, location);
    }
  }

  glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &params);
  logger->Log("GL_ACTIVE_UNIFORMS = %i", params);
  for (int i = 0; i < params; i++) {
    char name[64];
    int max_length = 64;
    int actual_length = 0;
    int size = 0;
    GLenum type;
    glGetActiveUniform(
        program,
        i,
        max_length,
        &actual_length,
        &size,
        &type,
        name
        );
    if (size > 1) {
      for (int j = 0; j < size; j++) {
        char long_name[64];
        sprintf(long_name, "%s[%i]", name, j);
        int location = glGetUniformLocation(program, long_name);
        logger->Log("  %i) type:%s name:%s location:%i",
            i, GLTypeToString(type), long_name, location);
      }
    } else {
      int location = glGetUniformLocation(program, name);
      logger->Log("  %i) type:%s name:%s location:%i",
          i, GLTypeToString(type), name, location);
    }
  }

  printProgramInfoLog(program, logger);
}

static const char* GLTypeToString(GLenum type) {
  switch (type) {
    case GL_BOOL: return "bool";
    case GL_INT: return "int";
    case GL_FLOAT: return "float";
    case GL_FLOAT_VEC2: return "vec2";
    case GL_FLOAT_VEC3: return "vec3";
    case GL_FLOAT_VEC4: return "vec4";
    case GL_FLOAT_MAT2: return "mat2";
    case GL_FLOAT_MAT3: return "mat3";
    case GL_FLOAT_MAT4: return "mat4";
    case GL_SAMPLER_2D: return "sampler2D";
    case GL_SAMPLER_3D: return "sampler3D";
    case GL_SAMPLER_CUBE: return "samplerCube";
    case GL_SAMPLER_2D_SHADOW: return "sampler2DShadow";
    default: break;
  }
  return "other";
}

static size_t bufferline(char* lineptr, size_t n, FILE * stream) {
  char *bufptr = NULL;
  char *p = bufptr;
  size_t size;
  int c;

  if (lineptr == NULL || n < 2 || stream == NULL) {
    return -1;
  }
  bufptr = lineptr;
  size = n;

  c = fgetc(stream);
  if (c == EOF) {
    return -1;
  }
  p = bufptr;
  while (c != EOF) {
    if ((p - bufptr) > (size - 2)) {
      *p++ = '\0';
      return size;
    }
    *p++ = c;
    if (c == '\n') {
      break;
    }
    c = fgetc(stream);
  }

  *p++ = '\0';

  return p - bufptr - 1;
}
