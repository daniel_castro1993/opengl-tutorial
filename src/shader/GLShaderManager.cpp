#include "shaders/shaderlib.h"

#include <mutex>

using namespace tuto1;
using namespace std;

static GLShaderManager* s_instance = NULL;
static mutex mtx;

GLShaderManager::GLShaderManager() {
}

GLShaderManager::~GLShaderManager() {
}

GLShaderProgram* GLShaderManager::NewProgram(string name) {
  map<string, GLShaderProgram*>::iterator it;

  it = _programs.find(name);

  if (it != _programs.end()) {
    delete it->second;
    _programs.erase(it);
  }

  _programs[name] = new GLShaderProgram(name);

  return _programs[name];
}

GLShaderProgram* GLShaderManager::GetProgram(string name) {
  map<string, GLShaderProgram*>::iterator it;

  it = _programs.find(name);

  if (it != _programs.end()) {
    return it->second;
  }

  return NULL;
}

void GLShaderManager::DelProgram(string name) {
  map<string, GLShaderProgram*>::iterator it;

  it = _programs.find(name);

  if (it != _programs.end()) {
    delete it->second;
    _programs.erase(it);
  }
}

void GLShaderManager::AddShader(string path, GLuint frag) {
  map<string, pair<GLuint, int> >::iterator it;

  it = _shaders.find(path);

  if (it != _shaders.end()) {
    // found
    it->second.first = frag;
    it->second.second++;
  } else {
    _shaders[path].first = frag;
    _shaders[path].second = 1;
  }
}

bool GLShaderManager::GetShader(string path, GLuint& vert) {
  map<string, pair<GLuint, int> >::iterator it;

  it = _shaders.find(path);

  if (it != _shaders.end()) {
    vert = it->second.first;
    it->second.second++;
    return true;
  }
  return false;
}

void GLShaderManager::DelShader(string path) {
  map<string, pair<GLuint, int> >::iterator it;

  it = _shaders.find(path);

  if (it != _shaders.end()) {
    it->second.second--;
    if (it->second.second == 0) {
      glDeleteShader(it->second.first);
      _shaders.erase(it);
    }
  }
}

GLShaderManager* GLShaderManager::instance() {
  if (s_instance == NULL) {
    mtx.lock();
    if (s_instance == NULL) {
      s_instance = new GLShaderManager();
    }
    mtx.unlock();
  }

  return s_instance;
}

