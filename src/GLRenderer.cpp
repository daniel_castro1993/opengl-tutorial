#include "GLRenderer.hpp"

#include <functional>
#include <mutex>

using namespace tuto1;
using namespace std;

static GLRenderer* s_instance = NULL;
static mutex mtx;

void logGLParams(GLLogger* logger);

GLRenderer::GLRenderer() : _isExit(false), _logger(GLLogger::instance()),
_window(NULL) {

  _logger->SetDefaultLevel(GLLoggerLevel::INFO);
  _logger->Log("starting GLFW");
  _logger->Log("%s", glfwGetVersionString());

  glfwSetErrorCallback([] (int error, const char* description) -> void {
    GLLogger* logger = GLLogger::instance();
    logger->SetDefaultLevel(GLLoggerLevel::ERROR);
    logger->Log("GLFW ERROR: code %i msg: %s\n", error, description);
  });

}

GLRenderer::~GLRenderer() {
}

bool GLRenderer::Prepare() {

  if (!glfwInit()) {
    _logger->SetDefaultLevel(GLLoggerLevel::ERROR);
    _logger->Log("ERROR: could not start GLFW3\n");
    return false;
  }

  // uncomment these lines if on Apple OS X
#ifdef __OPENGL_LOW_VERSION__
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

  //  glfwWindowHint(GLFW_SAMPLES, 16);

  if (_window == NULL) {
    SetWindowed();
  }

  // start GLEW extension handler
  glewExperimental = GL_TRUE;
  glewInit();

  const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
  const GLubyte* version = glGetString(GL_VERSION); // version as a string

  // get version info
  _logger->Log("Renderer: %s", renderer);
  _logger->Log("OpenGL version supported %s", version);

  logGLParams(_logger);

  // tell GL to only draw onto a pixel if the shape is closer to the viewer
  glEnable(GL_DEPTH_TEST); // enable depth-testing
  glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"

  return true;
}

bool GLRenderer::SetWindowed(int width, int height) {
  if (_window != NULL) {
    glfwDestroyWindow(_window);
  }
  _window = glfwCreateWindow(width, height, _title.c_str(), NULL, NULL);
  if (!_window) {
    _logger->SetDefaultLevel(GLLoggerLevel::ERROR);
    _logger->Log("ERROR: could not open window with GLFW3");
    glfwTerminate();
    return false;
  }
  glfwSetFramebufferSizeCallback(_window, []
          (GLFWwindow* window, int width, int height) -> void {
            glViewport(0, 0, width, height);
          });
  glfwMakeContextCurrent(_window);
}

bool GLRenderer::SetFullscreen() {
  int count_vmode;

  if (_window != NULL) {
    glfwDestroyWindow(_window);
  }

  _monitor = glfwGetPrimaryMonitor();
  _vmode = glfwGetVideoModes(_monitor, &count_vmode);
  _window = glfwCreateWindow(
          _vmode[count_vmode - 1].width, _vmode[count_vmode - 1].height,
          _title.c_str(), _monitor, NULL
          );
  glfwMakeContextCurrent(_window);
}

void GLRenderer::SetWindowTitle(string title) {
  _title = title;
  if (_window != NULL) {
    glfwSetWindowTitle(_window, title.c_str());
  }
}

list<string>& GLRenderer::GetVideoModes() {
  int i, count_vmode;
  if (_videoModes.size() == 0) {
    _vmode = glfwGetVideoModes(_monitor, &count_vmode);
    for (i = 0; i < count_vmode; ++i) {
      char vmodeStr[64];
      string vmode;

      sprintf(vmodeStr, "%ix%i@%iHz", _vmode[i].width, _vmode[i].height,
              _vmode[i].refreshRate);

      vmode = vmodeStr;
      _videoModes.push_back(vmodeStr);
    }
  }
}

GLFWwindow* GLRenderer::GetWindow() {
  return _window;
}

void GLRenderer::AddLoadingCallback(string name,
        function<void(GLRenderer&) > callback) {

  _loads[name] = callback;
}

void GLRenderer::AddCallback(string name,
        function<void(GLRenderer&, double) > callback) {

  _callbacks[name] = callback;
}

void GLRenderer::MainLoop() {

  map<string, function<void(GLRenderer&, double) > >::iterator it;
  map<string, function<void(GLRenderer&) > >::iterator it_l;

  _lastTS = glfwGetTime();

  for (it_l = _loads.begin(); it_l != _loads.end(); ++it_l) {
    it_l->second(*this);
  }

  while (!_isExit) {
    double ts, delta;

    for (it = _callbacks.begin(); it != _callbacks.end(); ++it) {
      ts = glfwGetTime();
      delta = ts - _lastTS;
      it->second(*this, delta);
    }
    _lastTS = ts;
  }
}

void GLRenderer::Exit() {
  _isExit = true;
}

GLRenderer* GLRenderer::instance() {
  if (s_instance == NULL) {
    mtx.lock();
    if (s_instance == NULL) {
      s_instance = new GLRenderer();
    }
    mtx.unlock();
  }

  return s_instance;
}

void logGLParams(GLLogger* logger) {
  logger->SetDefaultLevel(GLLoggerLevel::INFO);

  GLenum params[] = {
    GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS,
    GL_MAX_CUBE_MAP_TEXTURE_SIZE,
    GL_MAX_DRAW_BUFFERS,
    GL_MAX_FRAGMENT_UNIFORM_COMPONENTS,
    GL_MAX_TEXTURE_IMAGE_UNITS,
    GL_MAX_TEXTURE_SIZE,
    GL_MAX_VARYING_FLOATS,
    GL_MAX_VERTEX_ATTRIBS,
    GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS,
    GL_MAX_VERTEX_UNIFORM_COMPONENTS,
    GL_MAX_VIEWPORT_DIMS,
    GL_STEREO,
  };
  const char* names[] = {
    "GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS",
    "GL_MAX_CUBE_MAP_TEXTURE_SIZE",
    "GL_MAX_DRAW_BUFFERS",
    "GL_MAX_FRAGMENT_UNIFORM_COMPONENTS",
    "GL_MAX_TEXTURE_IMAGE_UNITS",
    "GL_MAX_TEXTURE_SIZE",
    "GL_MAX_VARYING_FLOATS",
    "GL_MAX_VERTEX_ATTRIBS",
    "GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS",
    "GL_MAX_VERTEX_UNIFORM_COMPONENTS",
    "GL_MAX_VIEWPORT_DIMS",
    "GL_STEREO",
  };
  logger->Log("GL Context Params:");

  for (int i = 0; i < 10; i++) {
    int v = 0;
    glGetIntegerv(params[i], &v);
    logger->Log("%s %i", names[i], v);
  }
  // others
  int v[2];
  v[0] = v[1] = 0;
  glGetIntegerv(params[10], v);
  logger->Log("%s %i %i", names[10], v[0], v[1]);
  unsigned char s = 0;
  glGetBooleanv(params[11], &s);
  logger->Log("%s %u", names[11], (unsigned int) s);
  logger->Log("-----------------------------");
}

