#include "GLLogger.hpp"

#include <time.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <mutex>

using namespace tuto1;
using namespace std;

static GLLogger* s_instance = NULL;
static mutex mtx;

bool startLogFile(FILE* fp, std::string fileName);
void printLevel(FILE* fp, GLLoggerLevel level);

GLLogger::GLLogger(string fileName) : _defaultLevel(GLLoggerLevel::INFO),
_printStdOut(true) {

  _fp = fopen(fileName.c_str(), "w+");
  assert(_fp != NULL);
  _fileName = fileName;
  startLogFile(_fp, _fileName.c_str());
}

GLLogger::~GLLogger() {
  if (_fp != NULL) {
    fclose(_fp);
  }
}

void GLLogger::SetOutputFile(string fileName) {

  if (_fp != NULL) {
    fclose(_fp);
  }

  _fp = fopen(fileName.c_str(), "w+");
  _fileName = fileName;
  assert(_fp != NULL);
  startLogFile(_fp, _fileName.c_str());
}

void GLLogger::SetDefaultLevel(GLLoggerLevel level) {
  _defaultLevel = level;
}

GLLoggerLevel GLLogger::GetDefaultLevel() {
  return _defaultLevel;
}

void GLLogger::SetPrintToConsole(bool printStdOut) {
  _printStdOut = printStdOut;
}

bool GLLogger::GetPrintToConsole() {
  return _printStdOut;
}

bool GLLogger::Log(const char* message, ...) {
  va_list argptr;

  if (!_fp) {
    fprintf(
        stderr,
        "ERROR: could not open GL_LOG_FILE %s file for appending\n",
        _fileName.c_str()
        );
    return false;
  }
  
  printLevel(_fp, _defaultLevel);
  va_start(argptr, message);
  vfprintf(_fp, message, argptr);
  va_end(argptr);
  fprintf(_fp, "\n");

  if (_printStdOut) {
    printLevel(stdout, _defaultLevel);
    va_start(argptr, message);
    vfprintf(stdout, message, argptr);
    va_end(argptr);
    fprintf(stdout, "\n");
  } else if (_defaultLevel == GLLoggerLevel::ERROR) {
    printLevel(stderr, _defaultLevel);
    va_start(argptr, message);
    vfprintf(stderr, message, argptr);
    va_end(argptr);
    fprintf(stderr, "\n");
  }
  
  fflush(_fp);
  fflush(stderr);
  fflush(stdout);
  
  return true;
}

GLLogger* GLLogger::instance() {
  if (s_instance == NULL) {
    mtx.lock();
    if (s_instance == NULL) {
      s_instance = new GLLogger(GLLOGGER_OUTPUT_FILE);
    }
    mtx.unlock();
  }

  return s_instance;
}

void printLevel(FILE* fp, GLLoggerLevel level) {
  const char* levelStr;
  switch (level) {
    case GLLoggerLevel::INFO:
      levelStr = "[INFO]";
      break;
    case GLLoggerLevel::WARNING:
      levelStr = "[WARNING]";
      break;
    case GLLoggerLevel::ERROR:
      levelStr = "[ERROR]";
      break;
    default:
      levelStr = "...";
  }
  fprintf(fp, "%10s: ", levelStr);
}

bool startLogFile(FILE* fp, string fileName) {
  if (!fp) {
    fprintf(stderr,
        "ERROR: could not open GL_LOG_FILE log file %s for writing\n",
        fileName.c_str());
    return false;
  }
  time_t now = time(NULL);
  char* date = ctime(&now);
  fprintf(fp, "\n");
  fprintf(fp, " ###############################%s### \n ##\n",
      string(25, '#').c_str());
  fprintf(fp, " ## GL_LOG_FILE log. local time %25s ##\n", date);
  fprintf(fp, " ###############################%s### \n",
      string(25, '#').c_str());
  fflush(fp);
  return true;
}

