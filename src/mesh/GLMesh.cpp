#include "mesh/GLMesh.hpp"

#include "glm/gtc/type_ptr.hpp"

#include <iostream>

#define BUFFER_OFFSET(i) ((char*)NULL + (i))

using namespace tuto1;
using namespace std;
using namespace glm;

GLMesh::GLMesh() : _useIndices(true) {
  glGenBuffers(1, &_vertVBO);
  glGenBuffers(1, &_indxVBO);
}

GLMesh::GLMesh(const GLMesh& orig) {
  _vertVBO = orig._vertVBO;
  _indxVBO = orig._indxVBO;
  _useIndices = orig._useIndices;
}

GLMesh::~GLMesh() {
  glDeleteBuffers(1, &_vertVBO);
  glDeleteBuffers(1, &_indxVBO);
}

void GLMesh::SetUseIndices(bool useIndices) {
  _useIndices = useIndices;
}

void GLMesh::AddVertex(vec3 pos, vec3 nor, vec2 tex) {
  _vertices.push_back({pos, nor, tex});
}

void GLMesh::AddVertex(vertex_t vert) {
  _vertices.push_back(vert);
}

void GLMesh::AddIndex(GLushort indx) {
  _indices.push_back(indx);
}

void GLMesh::GenerateVBO() {

  if (_useIndices) {
    glGenBuffers(1, &_vertVBO);
    glBindBuffer(GL_ARRAY_BUFFER, _vertVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof (vertex_t) * _vertices.size(),
            static_cast<GLfloat*> (&_vertices.front().position[0]),
            GL_STATIC_DRAW);
  } else {
    glBindBuffer(GL_ARRAY_BUFFER, _vertVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof (vertex_t) * _vertices.size(),
            static_cast<GLfloat*> (&_vertices.front().position[0]), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indxVBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof (GLushort) * _indices.size(),
            static_cast<GLushort*> (&_indices.front()), GL_STATIC_DRAW);
  }
}

void GLMesh::GenerateVAO() {
  if (_useIndices) {
    glBindBuffer(GL_ARRAY_BUFFER, _vertVBO);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof (vertex_t),
            BUFFER_OFFSET(0));
    
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof (vertex_t),
            BUFFER_OFFSET(sizeof (vec3)));
    
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof (vertex_t),
            BUFFER_OFFSET(2 * sizeof (vec3)));
  } else {
    glGenVertexArrays(1, &_VAO);
    glBindVertexArray(_VAO);
    glBindBuffer(GL_ARRAY_BUFFER, _vertVBO);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof (vertex_t),
            BUFFER_OFFSET(0));
    
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof (vertex_t),
            BUFFER_OFFSET(sizeof (vec3)));
    
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof (vertex_t),
            BUFFER_OFFSET(2 * sizeof (vec3)));
  }
}

void GLMesh::Draw() {
  if (_useIndices) {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indxVBO);
    glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_SHORT, BUFFER_OFFSET(0));
  } else {
    glBindVertexArray(_VAO);
    glDrawArrays(GL_TRIANGLES, 0, 3);
  }
}
